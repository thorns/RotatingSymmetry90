# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:48-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry90/test/KerrSchild-rotating-90.par"
#
# ADMBASE::betax (admbase::shift)
# 1:iteration 2:time 3:data
# data columns: 3:betax 4:betay 5:betaz
0 0 0.218851702633964 0.128277601446516 0.184430402780389
1 0.1 0.218855017839061 0.128379344164935 0.184455260911607
2 0.2 0.218845563559847 0.128550337802774 0.184426500990021
3 0.3 0.218826997951027 0.12870151755398 0.184263190174912
