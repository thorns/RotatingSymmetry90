# Scalar ASCII output created by CarpetIOScalar
# created on erik-schnetters-macbook-pro.local by eschnett on Jan 06 2012 at 10:37:48-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/CactusNumerical/RotatingSymmetry90/test/KerrSchild-rotating-90.par"
#
# ADMBASE::alp (admbase::lapse)
# 1:iteration 2:time 3:data
# data columns: 3:alp
0 0 0.346806738817669
1 0.1 1e-08
2 0.2 0.000954025976773804
3 0.3 0.000830636683043664
